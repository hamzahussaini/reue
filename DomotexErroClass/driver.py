import json
import csv
import datetime
import time
import pandas as pd
from collections import OrderedDict
from UserData import UserData
import re
import numpy as np
#import matplotlib.pylab as plt


tracker_name = "GestureTracker"
# csv_columns = ["timestamp", "x1", "y1", "x2", "y2", "scale", "scaleFactor", "single_tap", "single_tap_properties",
#                "fling", "fling_properties"]

#for tracking_example_domotex.json
csv_columns = ["timestamp", "TrackerType", "totalMemory", "appVersion", "screenBrightness", "actionType", "viewPointer", "TrackingId", "x1",
               "y1", "x2", "y2", "scale", "scaleFactor", "single_tap", "single_tap_properties",
               "fling", "fling_properties"]

session_threshold = 99000
global_session_number = 1


class ViewPointer:
    view_pointer_mapping = {}
    view_pointer_mapping_value = 0


class ActionType:
    action_type_mapping = {}
    action_type_mapping_value = 0


class TimestampNode:
    current_timestamp = 0.0
    next_timestamp = 0.0
    previous_timestamp_value = 0

    def reset_values(self):
        self.current_timestamp = 0.0
        self.next_timestamp = 0.0
        self.previous_timestamp_value = 0.0

    def set_next_timestamp(self, val):
        self.next_timestamp = val

    def reset_current_timestamp(self):
        self.current_timestamp = 0.0

    def reset_previous_timestamp(self):
        self.previous_timestamp_value = 0.0

    def reset_values_for_next_session(self):
        self.set_next_timestamp(self.current_timestamp)
        self.reset_previous_timestamp()


class UserSession:

    def __init__(self):
        self.user_data_list = []
        self.session_number = 1

    def get_last_timestamp(self):
        return self.user_data_list[-1].timestamp

    def is_next_session_needed(self, timestamp):
        if timestamp >= session_threshold:
            return True
        return False

    def add_to_user_data(self, user_data_obj):
        if not self.is_next_session_needed(user_data_obj.timestamp):
            self.user_data_list.append(user_data_obj)
            return True
        return False

    def get_user_data_list(self):
        return self.user_data_list

    def clear_user_data_list(self):
        self.user_data_list.clear()

    def update_session_number(self, session_number):
        self.session_number  = session_number

    def get_session_number(self):
        return self.session_number


class User:
    def __init__(self):
        self.user_sessions = []

    def add_session(self, user_session):
        self.user_sessions.append(user_session)

    def get_user_sessions(self):
        return self.user_sessions

    def get_last_user_session(self):
        return self.user_sessions[-1]


view_pointer_obj = ViewPointer()
action_type_obj = ActionType()
timestamp_node_obj = TimestampNode()

# all_users dictionary is going to have objects of type User
all_users = {}


def json_reader(json_filename):
    """
    reads json file and returns data
    :param json_filename: name of the json file
    :return: data in json format
    """
    json_data = json.load(open(json_filename, encoding="utf8"))
    return json_data


def normalize_timestamp(timestamp):
    """
    normalizes timestamp to human readable format
    :param timestamp: timestamp value
    :return: human readable time
    """
    norm_timestamp = float(timestamp)
    nlt = time.strftime("%a, %d %b %Y %H:%M:%S", time.localtime(norm_timestamp))
    return nlt


def get_view_pointer_substring(view_pointer_string):
    """
    slices out the substring needed from viewPointer value
    :param view_pointer_string: string to slice
    :return: sliced substring
    """
    updated_string = view_pointer_string
    colon_index = updated_string.find(":")
    if colon_index != -1:
        updated_string = updated_string[colon_index + 1:]
    return updated_string


def get_action_type_substring(action_type_string):
    """
    slices out the substring needed from actionType value
    :param action_type_string: string to slice
    :return: sliced substring
    """
    updated_string = action_type_string
    hash_index = updated_string.find("#")
    if hash_index != -1:
        updated_string = updated_string[:hash_index]
    return updated_string


def normalize_view_pointer(view_pointer_string):
    """
    normalize the substring of viewPointerValue and return the respective mapped value
    :param view_pointer_string: string to normalize
    :return: normalized string's mapped value
    """
    updated_string = get_view_pointer_substring(view_pointer_string)
    if updated_string not in view_pointer_obj.view_pointer_mapping:
        view_pointer_obj.view_pointer_mapping_value += 1
        view_pointer_obj.view_pointer_mapping[updated_string] = view_pointer_obj.view_pointer_mapping_value
    return str(view_pointer_obj.view_pointer_mapping_value)


def normalize_action_type(action_type_string):
    """
    normalize the substring of viewPointerValue and return the respective mapped value
    :param action_type_string: string to normalize
    :return: normalized string's mapped value
    """
    updated_string = get_action_type_substring(action_type_string)
    if updated_string not in action_type_obj.action_type_mapping:
        action_type_obj.action_type_mapping_value += 1
        action_type_obj.action_type_mapping[updated_string] = action_type_obj.action_type_mapping_value


def prepare_legends(json_data, main_key):
    """
    prepare legends for mapping and writes them to respective json files
    :param json_data: json data to parse
    :param main_key: main key to read data from
    :return: none
    """
    for data in json_data[main_key]:
        if value_exists(data, "actionType"):
            normalize_action_type(str(data["actionType"]))
        if value_exists(data, "viewPointer"):
            normalize_view_pointer(str(data["viewPointer"]))
        write_legend_to_files()


def value_exists(json_data, key_to_check):
    """
    check if value exists in json data
    :param json_data: data to search in
    :param key_to_check: value to find
    :return: value exists or not
    """
    return key_to_check in json_data


def parse_json_data_to_user(data):
    user_id = data['userId']

    trackerType_val = "0"
    if str(data["trackerType"]) == "GestureTracker":
        trackerType_val = "4"

    elif str(data["trackerType"]) == "DeviceInfoTracker":
        trackerType_val = "1"

    elif str(data["trackerType"]) == "LifeCycleTracker":
        trackerType_val = "3"

    elif str(data["trackerType"]) == "TouchEventTracker":
        trackerType_val = "2"

    total_memory_val = ""
    if value_exists(data, "totalMemory"):
        total_memory_val = data["totalMemory"]

    appVersion_val = ""
    if value_exists(data, "appVersion"):
        appVersion_val = data["appVersion"]

    actionType_val = ""
    if value_exists(data, "actionType"):
        action_type_substring = get_action_type_substring(str(data["actionType"]))
        if action_type_substring in action_type_obj.action_type_mapping:
            actionType_val = action_type_obj.action_type_mapping[action_type_substring]

    viewPointer_val = ""
    if value_exists(data, "viewPointer"):
        view_pointer_substring = get_view_pointer_substring(str(data["viewPointer"]))
        if view_pointer_substring in view_pointer_obj.view_pointer_mapping:
            viewPointer_val = view_pointer_obj.view_pointer_mapping[view_pointer_substring]

    # deviceHardwareModel_val = ""
    # if value_exists(data, "deviceHardwareModel"):
    #     appVersion_val = data["deviceHardwareModel"]

    screenBrightness_val = ""
    if value_exists(data, "screenBrightness"):
        screenBrightness_val = data["screenBrightness"]

    trackingId_val = ""
    xCoordinate_val = ""
    yCoordinate_val = ""
    if value_exists(data, "coordinate"):
        trackingId_val = data["coordinate"]["trackingId"]
        xCoordinate_val = data["coordinate"]["x"]
        yCoordinate_val = data["coordinate"]["y"]

    scale_value = ""
    if value_exists(data, "scale"):
        scale_value = data["scale"]

    timestamp_node_obj.current_timestamp = float(data["timestamp"])
    if timestamp_node_obj.next_timestamp == 0.0:
        # first timestamp
        timestamp_node_obj.next_timestamp = float(data["timestamp"])
    timestamp_val = int(
        (timestamp_node_obj.current_timestamp - timestamp_node_obj.next_timestamp) * 1000) \
        + timestamp_node_obj.previous_timestamp_value
    timestamp_node_obj.previous_timestamp_value = timestamp_val
    timestamp_node_obj.next_timestamp = timestamp_node_obj.current_timestamp

    return UserData(user_id, timestamp_val, trackerType_val, total_memory_val, appVersion_val, screenBrightness_val,
                    actionType_val, viewPointer_val, trackingId_val, xCoordinate_val, yCoordinate_val, "", "", "",
                    "", "", "", "", "")


def write_user_session_data_to_file():
    for user_id, user_obj in all_users.items():
        for session in user_obj.get_user_sessions():
            with open(user_id + " [SESSION " + str(session.get_session_number()) + "] " + ".csv", 'w', encoding='utf-8') \
                    as resultFile:
            # with open("[SESSION " + str(session.get_session_number()) + "] " + user_id + ".csv", 'w', encoding='utf-8') \
            #         as resultFile:
                wr = csv.writer(resultFile, dialect='excel', lineterminator='\n')
                wr.writerow(csv_columns)
                user_data_list = session.get_user_data_list()
                for user_data in user_data_list:
                    wr.writerow([user_data.timestamp, user_data.tracker_type, user_data.total_memory, user_data.app_version,
                    user_data.screen_brightness, user_data.action_type, user_data.view_pointer, user_data.tracking_id,
                                 user_data.x1, user_data.y1])
            resultFile.close()


def parse_data_file(json_data, main_key):
    user_ids = []
    for i in json_data[main_key]:
        user_ids.append(i['userId'])
        # easy user id 4C62589A-8F17-4844-A22D-7658FFDC152E (lower bound example)
        # difficult user id F5A755DE-ACA2-4BE0-95FB-DF22AF538AFE
         #user_ids.append("F5A755DE-ACA2-4BE0-95FB-DF22AF538AFE")
         #break
    unique_user_ids = list(set(user_ids))

    csv_files = []
    for i in unique_user_ids:
        csv_files.append(i + ".csv")
        #break

    while csv_files and unique_user_ids:
        csv_file_name = csv_files.pop(0)
        user_id = unique_user_ids.pop(0)

        for data in json_data[main_key]:
            if data['userId'] == user_id:
                user_obj = parse_json_data_to_user(data)
                if user_id not in all_users:
                    new_user = User()
                    # all_users[str(user_id)] = User()
                    current_user_session = UserSession()
                    current_user_session.add_to_user_data(user_obj)
                    new_user.add_session(current_user_session)
                    all_users[str(user_id)] = new_user
                    # all_users[str(user_id)].add_session(current_user_session)
                else:
                    current_user = all_users[user_id]
                    last_session = current_user.get_last_user_session()
                    if not last_session.add_to_user_data(user_obj):
                        # we need a new session
                        previous_session_number = last_session.get_session_number()
                        user_obj.reset_timestamp()
                        timestamp_node_obj.reset_values_for_next_session()
                        new_user_session = UserSession()
                        new_user_session.clear_user_data_list()
                        new_user_session.update_session_number(previous_session_number + 1)
                        current_user.add_session(new_user_session)
                        current_user.get_last_user_session().add_to_user_data(user_obj)
                        all_users[user_id] = current_user
        timestamp_node_obj.reset_values()
        write_user_session_data_to_file()
    return





def write_legend_to_files():
    with open('view_pointer_legend.json', 'w') as fp:
        json.dump(OrderedDict(view_pointer_obj.view_pointer_mapping), fp, sort_keys=True, indent=4, separators=(',', ": "))
    with open('action_type_legend.json', 'w') as fp:
        json.dump(OrderedDict(action_type_obj.action_type_mapping), fp, sort_keys=True, indent=4, separators=(',', ": "))


if __name__ == '__main__':
    # json_file = "tracking_example.json"
    # main_key = "tracking"
    json_file = "tracking_example_domotex.json"
    main_key = "tracking_example_domotex"
    json_data = json_reader(json_file)
    print("Preparing legend files...")
    prepare_legends(json_data, main_key)
    print("Parsing json data...")
    parse_data_file(json_data, main_key)

