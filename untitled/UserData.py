class UserData:
    def __init__(self, user_id, timestamp, tracker_type, total_memory, app_version, screen_brightness, action_Type,
                 view_pointer, tracking_id, x1, y1, x2, y2, scale, scale_factor, single_tap, single_tap_properties,
                 fling, fling_properties):
        super().__init__()
        self.user_id = user_id
        self.timestamp = timestamp
        self.tracker_type = tracker_type
        self.total_memory = total_memory
        self.app_version = app_version
        self.screen_brightness = screen_brightness
        self.action_type = action_Type
        self.view_pointer = view_pointer
        self.tracking_id = tracking_id
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.scale = scale
        self.scale_factor = scale_factor
        self.single_tap = single_tap
        self.single_tap_properties = single_tap_properties
        self.fling = fling
        self.fling_properties = fling_properties

    def reset_timestamp(self):
        self.timestamp = 0.0
